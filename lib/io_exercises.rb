# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  puts 'guess a number'
  guesses = 0
  random_number = rand(1..100)

  $stdin.each do |input|
    if input > random_number
      puts 'too high'
      guesses += 1
    elsif input < random_number
      puts 'too high'
      guesses += 1
    else
      break
    end
  end

  guesses
end

def shuffle_line

  puts 'write file name'
  filename = gets.chomp
  lines = File.readlines(filename)

  outfile = File.new('#{filename}-shuffled.txt', 'w')
  outfile.puts(lines.shuffle)
  outfile.close

end
